import 'antd/dist/antd.css'
import React from 'react'
import { Cookies } from 'react-cookie'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import 'unfetch/polyfill'
import App from './elements/App'
import { fetchAuthUser } from './modules/auth'
import { User } from './modules/users'
import * as serviceWorker from './serviceWorker'

const init = async () => {
  const cookies = new Cookies()
  const token = cookies.get('token')
  let user: User | undefined

  if (token) {
    ;({ data: user } = await fetchAuthUser(token))
  }

  ReactDOM.render(
    <BrowserRouter>
      <App auth={{ token, user }} />
    </BrowserRouter>,
    document.getElementById('root')
  )
}
init()

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
