import { API_URL } from '../config'

export async function fetchCurrencies() {
  return fetch(`${API_URL}/currencies`, { method: 'get' }).then(r => r.json())
}

export async function fetchCategories() {
  return fetch(`${API_URL}/categories`, { method: 'get' }).then(r => r.json())
}

export async function fetchCountries() {
  return fetch(`${API_URL}/countries`, { method: 'get' }).then(r => r.json())
}

export async function fetchCities() {
  return fetch(`${API_URL}/cities`, { method: 'get' }).then(r => r.json())
}
