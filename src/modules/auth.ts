import { API_URL } from '../config'

export async function fetchAuthUser(token: string) {
  return fetch(`${API_URL}/auth/user`, {
    method: 'get',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then(r => r.json())
}

export async function signIn(values) {
  return fetch(`${API_URL}/auth/sign-in`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(res => res.json())
}

export async function signUp(values) {
  return fetch(`${API_URL}/auth/sign-up`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(res => res.json())
}
