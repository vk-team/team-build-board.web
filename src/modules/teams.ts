import qs from 'qs'
import { API_URL } from '../config'

export async function fetchTeams(data: { query?: any; token?: string } = {}) {
  const headers: any = {}
  if (data.token) {
    headers.Authorization = `Bearer ${data.token}`
  }
  return fetch(`${API_URL}/teams?${qs.stringify(data.query)}`, {
    headers,
  }).then(res => res.json())
}

export async function createTeam(values, token: string) {
  return fetch(`${API_URL}/teams`, {
    method: 'post',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(r => r.json())
}

export async function deleteTeamById(id: number, token: string) {
  return fetch(`${API_URL}/teams/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    method: 'delete',
  }).then(res => res.json())
}

export async function fetchTeamById(id: number, token: string) {
  return fetch(`${API_URL}/teams/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then(res => res.json())
}

export async function updateTeamById(id: number, values, token: string) {
  return fetch(`${API_URL}/teams/${id}`, {
    method: 'put',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(r => r.json())
}
