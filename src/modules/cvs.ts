import qs from 'qs'
import { API_URL } from '../config'

export async function fetchCvs(data: { query?: any; token?: string } = {}) {
  const headers: any = {}
  if (data.token) {
    headers.Authorization = `Bearer ${data.token}`
  }
  return fetch(`${API_URL}/cv?${qs.stringify(data.query)}`, { headers }).then(res => res.json())
}

export async function fetchCvById(id: number, token: string) {
  return fetch(`${API_URL}/cv/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then(res => res.json())
}

export async function deleteCvById(id: number, token: string) {
  return fetch(`${API_URL}/cv/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    method: 'delete',
  }).then(res => res.json())
}

export async function createCv(values, token: string) {
  return fetch(`${API_URL}/cv`, {
    method: 'post',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(r => r.json())
}

export async function updateCvById(id: number, values, token: string) {
  return fetch(`${API_URL}/cv/${id}`, {
    method: 'put',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(values),
  }).then(r => r.json())
}
