import React from 'react'
import { useQuery } from 'react-query'
import { fetchCvs } from '../../../modules/cvs'
import { fetchTeams } from '../../../modules/teams'
import CvCard from '../../components/CvCard'
import TeamCard from '../../components/TeamCard'

const Home = () => {
  const cvsQuery = useQuery('cvs', () => fetchCvs({ query: { take: 5 } }))
  const teamsQuery = useQuery('teams', () => fetchTeams({ query: { take: 5 } }))

  return (
    <div>
      <h2>Top Cvs</h2>
      {cvsQuery.data && cvsQuery.data.data.map(c => <CvCard data={c} key={c.id} />)}
      <h2>Top Teams</h2>
      {teamsQuery.data && teamsQuery.data.data.map(t => <TeamCard data={t} key={t.id} />)}
    </div>
  )
}

export default Home
