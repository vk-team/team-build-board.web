import _ from 'lodash'
import React from 'react'
import { useMutation, useQuery } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import { fetchCvById, updateCvById } from '../../../../modules/cvs'
import { authContext } from '../../../context'
import CvForm from './CvForm'

function serializeForForm(data) {
  const formData = _.omit(data, ['categories', 'user', 'city', 'country', 'currency'])
  if (data.categories) {
    formData.categoriesIds = data.categories.map(c => c.id)
  }
  return formData
}

const CvUpdate = () => {
  const auth = React.useContext(authContext)
  const { id } = useParams()
  const history = useHistory()
  const [updateCvMutation] = useMutation(values =>
    updateCvById(parseInt(id!, 10), values, auth.token!)
  )
  const cvsQuery = useQuery('user-cv', () => fetchCvById(parseInt(id!, 10), auth.token!), {
    cacheTime: 0,
  })
  const handleSubmit = React.useCallback(async values => {
    await updateCvMutation(values)
    history.push('/user/cvs')
  }, [])

  if (!cvsQuery.data) {
    return null
  }

  return (
    <CvForm
      submitLabel="Update"
      onSubmit={handleSubmit}
      defaultValues={serializeForForm(cvsQuery.data.data)}
    />
  )
}

export default CvUpdate
