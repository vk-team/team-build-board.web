import React from 'react'
import { Route, Switch } from 'react-router-dom'
import CvCreate from './CvCreate'
import CvsList from './CvsList'
import CvUpdate from './CvUpdate'

const Cvs = () => {
  return (
    <>
      <h3>Cvs</h3>
      <Switch>
        <Route exact path="/user/cvs">
          <CvsList />
        </Route>
        <Route path="/user/cvs/create">
          <CvCreate />
        </Route>
        <Route path="/user/cvs/update/:id">
          <CvUpdate />
        </Route>
      </Switch>
    </>
  )
}

export default Cvs
