import React from 'react'
import { useMutation } from 'react-query'
import { useHistory } from 'react-router-dom'
import { createCv } from '../../../../modules/cvs'
import { authContext } from '../../../context'
import CvForm from './CvForm'

const CVCreate = () => {
  const auth = React.useContext(authContext)
  const history = useHistory()
  const [createCvMutation] = useMutation(values => createCv(values, auth.token!))
  const handleSubmit = React.useCallback(async values => {
    await createCvMutation(values)
    history.push('/user/cvs')
  }, [])

  return <CvForm submitLabel="Create" onSubmit={handleSubmit} defaultValues={{ enabled: true }} />
}

export default CVCreate
