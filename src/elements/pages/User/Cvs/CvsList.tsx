import React from 'react'
import { useMutation, useQuery } from 'react-query'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { deleteCvById, fetchCvs } from '../../../../modules/cvs'
import CvCard from '../../../components/CvCard'
import { authContext } from '../../../context'

const Nav = styled.nav`
  & ul {
    display: flex;
    list-style: none;

    & li:not(:first-child) {
      margin-left: 25px;
    }
  }
`

const CvsList = () => {
  const auth = React.useContext(authContext)
  const cvsQuery = useQuery('user-cvs', () =>
    fetchCvs({ query: { userId: auth.user!.id }, token: auth.token })
  )
  const [removeCvMutation] = useMutation((params: { id: number }) =>
    deleteCvById(params.id, auth.token!)
  )
  const removeCv = React.useCallback(async (id: number) => {
    await removeCvMutation({ id })
    cvsQuery.refetch()
  }, [])

  return (
    <>
      <Nav>
        <ul>
          <li>
            <Link to="/user/cvs/create">Create</Link>
          </li>
        </ul>
      </Nav>
      {cvsQuery.data &&
        cvsQuery.data.data.map(c => (
          <article key={c.id}>
            <CvCard data={c} />
            <Nav>
              <ul>
                <li>
                  <Link to={`/user/cvs/update/${c.id}`}>Update</Link>
                </li>
                <li onClick={() => removeCv(c.id)}>Delete</li>
              </ul>
            </Nav>
          </article>
        ))}
    </>
  )
}

export default CvsList
