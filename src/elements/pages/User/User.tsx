import React from 'react'
import { Link, Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import { authContext } from '../../context'
import Cvs from './Cvs'
import Teams from './Teams'

const Nav = styled.nav`
  display: flex;

  & ul {
    display: flex;
    list-style: none;

    & li:not(:first-child) {
      margin-left: 25px;
    }
  }
`

const User = () => {
  const auth = React.useContext(authContext)
  return (
    <>
      <h2>
        User:
        <i>{auth.user!.fullName}</i>
      </h2>
      <Nav>
        <ul>
          <li>
            <Link to="/user">User</Link>
          </li>
          <li>
            <Link to="/user/cvs">Cvs</Link>
          </li>
          <li>
            <Link to="/user/teams">Teams</Link>
          </li>
        </ul>
      </Nav>
      <Switch>
        <Route path="/user/cvs">
          <Cvs />
        </Route>
        <Route path="/user/teams">
          <Teams />
        </Route>
      </Switch>
    </>
  )
}

export default User
