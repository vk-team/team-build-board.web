import React from 'react'
import { useMutation } from 'react-query'
import { useHistory } from 'react-router-dom'
import { createTeam } from '../../../../modules/teams'
import { authContext } from '../../../context'
import TeamFrom from './TeamForm'

const TeamCreate = () => {
  const auth = React.useContext(authContext)
  const history = useHistory()
  const [createTeamMutation] = useMutation(values => createTeam(values, auth.token!))
  const handleSubmit = React.useCallback(async values => {
    await createTeamMutation(values)
    history.push('/user/teams')
  }, [])

  return <TeamFrom submitLabel="Create" onSubmit={handleSubmit} defaultValues={{ enabled: true }} />
}

export default TeamCreate
