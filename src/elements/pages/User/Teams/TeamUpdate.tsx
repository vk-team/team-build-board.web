import _ from 'lodash'
import React from 'react'
import { useMutation, useQuery } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import { fetchTeamById, updateTeamById } from '../../../../modules/teams'
import { authContext } from '../../../context'
import TeamForm from './TeamForm'

function serializeForForm(data) {
  const formData = _.omit(data, ['categories', 'owner', 'city', 'country', 'users', 'ownerId'])

  if (data.users) {
    formData.usersIds = data.users.map(u => u.id)
  }
  if (data.categories) {
    formData.categoriesIds = data.categories.map(c => c.id)
  }

  return formData
}

const TeamUpdate = () => {
  const auth = React.useContext(authContext)
  const { id } = useParams()
  const history = useHistory()
  const [updateTeamMutation] = useMutation(values =>
    updateTeamById(parseInt(id!, 10), values, auth.token!)
  )
  const teamQuery = useQuery('user-team', () => fetchTeamById(parseInt(id!, 10), auth.token!), {
    cacheTime: 0,
  })
  const handleSubmit = React.useCallback(async values => {
    await updateTeamMutation(values)
    history.push('/user/teams')
  }, [])

  if (!teamQuery.data) {
    return null
  }

  return (
    <TeamForm
      submitLabel="Update"
      onSubmit={handleSubmit}
      defaultValues={serializeForForm(teamQuery.data.data)}
    />
  )
}

export default TeamUpdate
