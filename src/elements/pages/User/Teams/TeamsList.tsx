import React from 'react'
import { useMutation, useQuery } from 'react-query'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { deleteTeamById, fetchTeams } from '../../../../modules/teams'
import TeamCard from '../../../components/TeamCard'
import { authContext } from '../../../context'

const Nav = styled.nav`
  & ul {
    display: flex;
    list-style: none;

    & li:not(:first-child) {
      margin-left: 25px;
    }
  }
`

const TeamsList = () => {
  const auth = React.useContext(authContext)
  const teamsQuery = useQuery('user-temas', () =>
    fetchTeams({ query: { ownerId: auth.user!.id }, token: auth.token })
  )
  const [removeTeamMutation] = useMutation((params: { id: number }) =>
    deleteTeamById(params.id, auth.token!)
  )
  const removeTeam = React.useCallback(async (id: number) => {
    await removeTeamMutation({ id })
    teamsQuery.refetch()
  }, [])

  return (
    <>
      <Nav>
        <ul>
          <li>
            <Link to="/user/teams/create">Create</Link>
          </li>
        </ul>
      </Nav>
      {teamsQuery.data &&
        teamsQuery.data.data.map(t => (
          <article key={t.id}>
            <TeamCard data={t} />
            <Nav>
              <ul>
                <li>
                  <Link to={`/user/teams/update/${t.id}`}>Update</Link>
                </li>
                <li onClick={async () => removeTeam(t.id)}>Delete</li>
              </ul>
            </Nav>
          </article>
        ))}
    </>
  )
}

export default TeamsList
