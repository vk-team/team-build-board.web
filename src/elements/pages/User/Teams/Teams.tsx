import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TeamCreate from './TeamCreate'
import TeamsList from './TeamsList'
import TeamUpdate from './TeamUpdate'

const Teams = () => {
  return (
    <>
      <h3>Teams</h3>
      <Switch>
        <Route exact path="/user/teams">
          <TeamsList />
        </Route>
        <Route path="/user/teams/create">
          <TeamCreate />
        </Route>
        <Route path="/user/teams/update/:id">
          <TeamUpdate />
        </Route>
      </Switch>
    </>
  )
}

export default Teams
