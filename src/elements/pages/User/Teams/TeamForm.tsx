import { Button } from 'antd'
import { Form, Formik } from 'formik'
import _ from 'lodash'
import React from 'react'
import { useQuery } from 'react-query'
import { fetchCategories, fetchCities, fetchCountries } from '../../../../modules/common'
import { CheckboxField, InputField, SelectField, TextField } from '../../../components/Form'

type Props = {
  submitLabel: string
  onSubmit: any
  defaultValues?: any
}

const TeamForm = (props: Props) => {
  const categoriesQuery = useQuery('categories', fetchCategories)
  const countriesQuery = useQuery('countries', fetchCountries)
  const citiesQuery = useQuery('cities', fetchCities)

  return (
    <Formik initialValues={props.defaultValues} onSubmit={props.onSubmit}>
      {formikProps => (
        <Form>
          <InputField type="text" name="name" label="Name" />

          <SelectField
            label="Categories"
            name="categoriesIds"
            mode="multiple"
            options={_.get(categoriesQuery, 'data.data', []).map(d => ({
              label: d.name,
              value: d.id,
            }))}
          />
          <SelectField
            name="countryId"
            label="Country"
            options={_.get(countriesQuery, 'data.data', []).map(d => ({
              label: d.name,
              value: d.id,
            }))}
          />
          <SelectField
            name="cityId"
            label="City"
            disabled={!formikProps.values.countryId}
            options={_.get(citiesQuery, 'data.data', [])
              .filter(c => c.countryId === formikProps.values.countryId)
              .map(d => ({
                label: d.name,
                value: d.id,
              }))}
          />

          <CheckboxField name="enabled" label="Enabled" />

          <TextField name="description" label="Description" />

          <Button type="primary" htmlType="submit">
            {props.submitLabel}
          </Button>
        </Form>
      )}
    </Formik>
  )
}

export default TeamForm
