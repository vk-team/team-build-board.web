import { Button } from 'antd'
import { Form as FormikForm, Formik } from 'formik'
import React, { useCallback } from 'react'
import * as yup from 'yup'
import { useCookies } from 'react-cookie'
import { useMutation } from 'react-query'
import { useHistory } from 'react-router-dom'
import { signUp } from '../../../modules/auth'
import { InputField } from '../../components/Form'
import * as validation from '../../../helpers/validation'

type Props = {
  setAuth: any
}

const validationSchema = yup.object({
  fullName: validation.isFullName,
  email: validation.isEmail,
  password: validation.isPassword,
  passwordRepeat: validation.isPasswordRepeat,
})

const SignUp = (props: Props) => {
  const history = useHistory()
  const [signUpMutation] = useMutation(signUp)
  const [, setCookie] = useCookies()
  const handleSubmit = useCallback(async values => {
    const res = await signUpMutation(values)
    setCookie('token', res.data.token)
    props.setAuth(res.data)
    history.push('/')
  }, [])

  return (
    <div>
      <h2>SignUp</h2>
      <Formik
        onSubmit={handleSubmit}
        initialValues={{ fullName: '', email: '', password: '', passwordRepeat: '' }}
        validationSchema={validationSchema}>
        <FormikForm>
          <InputField name="fullName" type="text" label="Full Name" />
          <InputField name="email" type="text" label="Email" />
          <InputField name="password" type="password" label="Password" />
          <InputField name="passwordRepeat" type="password" label="Repeat Password" />
          <Button type="primary" htmlType="submit">
            Sign Up
          </Button>
        </FormikForm>
      </Formik>
    </div>
  )
}

export default SignUp
