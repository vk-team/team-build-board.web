import { Button } from 'antd'
import { Form as FormikForm, Formik } from 'formik'
import React, { useCallback } from 'react'
import * as yup from 'yup'
import { useCookies } from 'react-cookie'
import { useMutation } from 'react-query'
import { useHistory } from 'react-router-dom'
import { signIn } from '../../../modules/auth'
import { InputField } from '../../components/Form'
import * as validation from '../../../helpers/validation'

type Props = {
  setAuth: any
}

const validationSchema = yup.object({
  email: validation.isEmail,
  password: validation.isPassword,
})

const SignIn = (props: Props) => {
  const history = useHistory()
  const [signInMutation] = useMutation(signIn)
  const [, setCookie] = useCookies()
  const handleSubmit = useCallback(async values => {
    const res = await signInMutation(values)
    setCookie('token', res.data.token)
    props.setAuth(res.data)
    history.push('/')
  }, [])

  return (
    <div>
      <h2>SignIn</h2>
      <Formik
        onSubmit={handleSubmit}
        initialValues={{ email: '', password: '' }}
        validationSchema={validationSchema}>
        <FormikForm>
          <InputField name="email" type="text" label="Email" />
          <InputField name="password" type="password" label="Password" />
          <Button type="primary" htmlType="submit">
            Sign In
          </Button>
        </FormikForm>
      </Formik>
    </div>
  )
}

export default SignIn
