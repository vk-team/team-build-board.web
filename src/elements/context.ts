import React from 'react'
import { User } from '../modules/users'

export type AuthContext = {
  user?: User
  token?: string
}
export const authContext = React.createContext<AuthContext>({})
