import React from 'react'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import { authContext, AuthContext } from './context'
import AppLayout from './layouts/AppLayout'
import Home from './pages/Home'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import User from './pages/User'
import { GlobalStyle } from './styled/global-styles'

const AppWrapper = styled.section`
  width: 80%;
  margin: 0 auto;
`

type Props = {
  auth: AuthContext
}

const App = (props: Props) => {
  const [authState, setAuth] = React.useState(props.auth)

  return (
    <AppWrapper>
      <GlobalStyle />
      <authContext.Provider value={authState}>
        <AppLayout user={authState.user}>
          <Switch>
            <Route path="/" exact>
              <Home />
            </Route>
            <Route path="/sign-in">
              <SignIn setAuth={setAuth} />
            </Route>
            <Route path="/sign-up">
              <SignUp setAuth={setAuth} />
            </Route>
            <Route path="/user">
              <User />
            </Route>
          </Switch>
        </AppLayout>
      </authContext.Provider>
    </AppWrapper>
  )
}

export default App
