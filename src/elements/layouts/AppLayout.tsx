import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100px;
`
const Nav = styled.nav`
  display: flex;

  & ul {
    display: flex;
    list-style: none;

    & li:not(:last-child) {
      margin-right: 40px;
    }
  }
`

type Props = {
  children: React.ReactElement
  user?: any
}

const AppLayout = (props: Props) => {
  return (
    <>
      <Header>
        <h1>Team Build Board</h1>
        <Nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            {props.user ? (
              <li>
                <Link to="/user">User</Link>
              </li>
            ) : (
              <>
                <li>
                  <Link to="/sign-in">Sign In</Link>
                </li>
                <li>
                  <Link to="/sign-up">Sign Up</Link>
                </li>
              </>
            )}
          </ul>
        </Nav>
      </Header>
      <main>{props.children}</main>
    </>
  )
}

export default AppLayout
