import { Descriptions } from 'antd'
import React from 'react'

type Props = {
  data: any
}

const TeamCard = (props: Props) => {
  return (
    <Descriptions title={props.data.name}>
      <Descriptions.Item label="Description">{props.data.description}</Descriptions.Item>
    </Descriptions>
  )
}

export default TeamCard
