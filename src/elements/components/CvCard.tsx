import { Descriptions } from 'antd'
import React from 'react'

type Props = {
  data: any
}

const CvCard = (props: Props) => {
  return (
    <Descriptions title={props.data.title}>
      <Descriptions.Item label="Description">{props.data.description}</Descriptions.Item>
    </Descriptions>
  )
}

export default CvCard
