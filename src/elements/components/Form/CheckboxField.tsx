import { Checkbox, Form } from 'antd'
import { Field } from 'formik'
import { get } from 'lodash'
import React from 'react'

const CheckboxComponent = ({ field, form, ...props }) => {
  const error = get(form.touched, field.name) && get(form.errors, field.name)

  return (
    <Form.Item label={props.label} validateStatus={error ? 'error' : ''} help={error}>
      <Checkbox {...field} {...props} checked={Boolean(field.value)} />
    </Form.Item>
  )
}

type Props = {
  name: string
  label?: string
}

const CheckboxField = (props: Props) => <Field {...props} component={CheckboxComponent} />

export default CheckboxField
