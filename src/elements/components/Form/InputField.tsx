import { Form, Input } from 'antd'
import { useField } from 'formik'
import React from 'react'

const InputField = props => {
  const [field, meta] = useField(props)
  const placeholder = props.placeholder || props.label.toLowerCase()

  return (
    <Form.Item
      label={props.label}
      validateStatus={meta.touched && meta.error ? 'error' : ''}
      help={meta.touched && meta.error ? meta.error : ''}>
      <Input {...field} {...props} placeholder={placeholder} />
    </Form.Item>
  )
}

export default InputField
