import { Form, Input } from 'antd'
import { Field } from 'formik'
import { get } from 'lodash'
import React from 'react'

const { TextArea } = Input

const TextComponent = ({ field, form, ...props }) => {
  const error = get(form.touched, field.name) && get(form.errors, field.name)

  return (
    <Form.Item label={props.label} validateStatus={error ? 'error' : ''} help={error}>
      <TextArea {...field} {...props} />
    </Form.Item>
  )
}

type Props = {
  name: string
  label?: string
}

const TextField = (props: Props) => <Field {...props} component={TextComponent} />

export default TextField
