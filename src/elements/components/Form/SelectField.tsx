import { Form, Select } from 'antd'
import { Field } from 'formik'
import { get } from 'lodash'
import React from 'react'

const SelectComponent = ({ field, form, ...props }) => {
  const error = get(form.touched, field.name) && get(form.errors, field.name)

  return (
    <Form.Item label={props.label} validateStatus={error ? 'error' : ''} help={error}>
      <Select
        {...field}
        {...props}
        onChange={value => form.setFieldValue(field.name, value)}
        onBlur={value => form.setFieldValue(field.name, value)}>
        {props.options &&
          props.options.map(o => (
            <Select.Option key={o.value} value={o.value}>
              {o.label}
            </Select.Option>
          ))}
      </Select>
    </Form.Item>
  )
}

type Props = {
  name: string
  label?: string
  options?: { value: string; label: string | number }[]
  mode?: 'default' | 'multiple' | 'tags'
  disabled?: boolean
}

const SelectField = (props: Props) => <Field {...props} component={SelectComponent} />

export default SelectField
