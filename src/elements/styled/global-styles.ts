import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  *,*::after,*::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }

  html {
    font-size: 62.5%;
  }

  body {
    font-size: 1.6rem;
    font-family: Roboto, sans-serif;
    font-weight: 400;
    color: #111111;
    box-sizing: border-box;
    background-color: #F6F6F6;
  }
`
