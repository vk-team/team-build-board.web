import * as yup from 'yup'

export const isEmail = yup
  .string()
  .email()
  .required()

export const isPasswordRepeat = yup
  .string()
  .min(2)
  .oneOf([yup.ref('password'), null], 'Passwords must match')
  .required()

export const isPassword = yup
  .string()
  .min(2)
  .required()

export const isFullName = yup
  .string()
  .min(10)
  .required()
